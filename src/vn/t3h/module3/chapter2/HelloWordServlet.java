package vn.t3h.module3.chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet("/page/chapter2/helloworld.html")
public class HelloWordServlet extends GenericServlet {

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html;charset=utf-8");
		try (PrintWriter pw = res.getWriter()) {
			pw.write("<h2>Hello World!</h2>");
		}

	}

}
