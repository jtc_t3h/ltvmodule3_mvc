package vn.t3h.module3.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

import vn.t3h.module3.chapter4.model.dao.ProductRepository;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/page/chapter4/product.html")
public class ProductController extends GenericServlet {
	
	private int size = 8;
	ProductRepository repository = new ProductRepository();

	private int getPage(int total) {
		return (int) Math.ceil(total / (float) size);
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		int p = 1;
		if (req.getParameter("p") != null) {
			p = Integer.parseInt(req.getParameter("p"));
		}
		try {
			req.setAttribute("n", getPage(repository.count()));
			req.setAttribute("list", repository.getProducts(p, size));
			req.getRequestDispatcher("/page/chapter4/produc-list.jsp").forward(req, res);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	

}
