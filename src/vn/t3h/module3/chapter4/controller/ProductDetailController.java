package vn.t3h.module3.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

import vn.t3h.module3.chapter4.model.dao.ProductRepository;

/**
 * Servlet implementation class ProductDetailController
 */
@WebServlet("/page/chapter4/product-detail.html")
public class ProductDetailController extends GenericServlet {
	ProductRepository repository = new ProductRepository();

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			
			request.setAttribute("o", repository.getProduct(id));
			request.getRequestDispatcher("/page/chapter4/product-detail.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
