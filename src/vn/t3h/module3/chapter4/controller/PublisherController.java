package vn.t3h.module3.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.module3.chapter4.model.dao.PublisherRepository;

/**
 * Servlet implementation class PublisherController
 */
@WebServlet("/page/chapter4/publisher.html")
public class PublisherController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PublisherRepository repository = new PublisherRepository();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setAttribute("list", repository.getPublishers());
			request.getRequestDispatcher("/page/chapter4/index.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
