package vn.t3h.module3.chapter4.model.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import vn.t3h.module3.chapter4.model.entity.Publisher;

public class PublisherRepository extends Repository {

	public List<Publisher> getPublishers() throws SQLException {
		try {
			open();
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Publisher");
			
			List<Publisher> list = new LinkedList<>();

			while (rs.next()) {
				list.add(new Publisher(rs.getInt("PublisherId"), rs.getString("PublisherName")));
			}
			return list;
		} finally {
			close();
		}
	}

	public int add(Publisher publisher) throws SQLException {
		int count = 0;
		try {
			open();
			pstmt = connection.prepareStatement("insert into Publisher(PublisherName) values (?)");
			pstmt.setString(1, publisher.getName());
			
			count = pstmt.executeUpdate();
		} finally {
			close();
		}
		return count;
	}
	
	
}
